package bridge.pen;

import bridge.Color;

public abstract class Pen {
//    沾颜色
    protected Color color;
    public void setColor(Color color){
        this.color=color;
    }
//    画画
   public  abstract  void  draw();
}
