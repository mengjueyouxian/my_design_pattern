package bridge.pen;

import bridge.Color;

public class Pen1 extends PenAbstract {
    public Pen1(Color color) {
        super(color);
    }

    public void draw() {
        System.out.println("用神笔的"+color.myColor()+"颜色画画");
    }
}
