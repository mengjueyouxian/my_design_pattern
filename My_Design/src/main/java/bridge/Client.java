package bridge;

import bridge.color.Red;
import bridge.pen.BigPen;
import bridge.pen.Pen;
import bridge.pen.Pen1;
import bridge.pen.PenAbstract;

/*思考：
    为什么要使用set。而不是 组合？
        在构造参数中使用也是可以的。

问题2：抽象类不能实例化的。怎么传入构造参数？
* */
public class Client {
    public static void main(String[] args) {
//        使用红色
        Color t = new Red();
//        大笔画
        Pen pen = new BigPen();
        pen.setColor(t);
//        画
        pen.draw();
//        第二种写法：
        PenAbstract penAbstract = new Pen1(t);
        penAbstract.draw();
    }


}
