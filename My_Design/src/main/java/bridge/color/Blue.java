package bridge.color;

import bridge.Color;

public class Blue implements Color
{
    public String myColor() {
        return "蓝色";
    }
}
