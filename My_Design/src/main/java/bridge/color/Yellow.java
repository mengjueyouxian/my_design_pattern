package bridge.color;

import bridge.Color;

public class Yellow implements Color
{
    public String myColor() {
        return "黄色";
    }
}
