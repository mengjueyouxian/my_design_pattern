package bridge.color;

import bridge.Color;

public class Red implements Color
{
    public String myColor() {
        return "红色";
    }
}
