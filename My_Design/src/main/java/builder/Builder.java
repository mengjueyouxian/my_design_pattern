package builder;

import builder.house.basics.Foundation;
import builder.house.basics.Renovation;
import builder.house.basics.Roof;
import builder.house.basics.Wall;

//开始建造房子
public class Builder implements  TerrainBuilder {

    public Foundation createFoundation() {
        return new Foundation(10);
    }

    public Renovation createRenovation() {
        return new Renovation("蓝色的装修");
    }

    public Roof createRoof() {
        return new Roof(1000,1000);
    }

    public Wall createWall() {
        return new Wall(1000,1000,1000);
    }
//指挥建造
    public House builder() {
        House house=new House();
        house.foundation=createFoundation();
        house.renovation=createRenovation();
        house.roof=createRoof();
        house.wall=createWall();
        return house;
    }
}
