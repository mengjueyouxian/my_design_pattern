package builder;

import builder.house.basics.Foundation;
import builder.house.basics.Renovation;
import builder.house.basics.Roof;
import builder.house.basics.Wall;

//抽象建造者
public interface TerrainBuilder {
    public Foundation createFoundation();
    public Renovation createRenovation();
    public Roof createRoof();
    public Wall createWall();
//    最后组装成产品
    public House  builder();

}
