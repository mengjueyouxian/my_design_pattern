package builder.house.basics;

//地基
public class Foundation {
    //    深度 默认打五米
    public int Depth = 5;

    public Foundation() {
    }

    public Foundation(int depth) {
        Depth = depth;
    }
}
