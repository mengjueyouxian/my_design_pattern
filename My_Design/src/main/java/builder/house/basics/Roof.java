package builder.house.basics;

public class Roof {
    //    长，宽(单位厘米)
    //    长
    public int forte = 3000;
    //    宽
    public int width = 1000;

    public Roof() {
    }

    public Roof(int forte, int width) {
        this.forte = forte;
        this.width = width;
    }
}
