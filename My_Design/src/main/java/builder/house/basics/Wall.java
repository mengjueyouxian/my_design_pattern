package builder.house.basics;

//墙壁
public class Wall {
    //    长，宽，高 (单位厘米)
//    长
    public int forte = 3000;
    //    宽
    public int width = 1000;
    //    高
    public int hight = 300;

    //默认
    public Wall() {
    }

    public Wall(int forte, int width, int hight) {
        this.forte = forte;
        this.width = width;
        this.hight = hight;
    }
}

