package builder.variation;

public class Person {
    /*名字（必须）*/
    private String name;
    /*性别（必须）*/
    private String gender;
    /*年龄（非必须）*/
    private String age;
    /*鞋子（非必须）*/
    private String shoes;
    /*衣服（非必须）*/
    private String clothes;
    /*钱（非必须）*/
    private String money;
    /*房子（非必须）*/
    private String house;
    /*汽车（非必须）*/
    private String car;
    /*职业（非必须）*/
    private String career;

    //    够着参数传入为建造者
    private Person(BuliderPerson builder) {
        this.name = builder.name;
        this.gender = builder.gender;
        this.age = builder.age;
        this.shoes = builder.shoes;
        this.clothes = builder.clothes;
        this.money = builder.money;
        this.house = builder.house;
        this.car = builder.car;
        this.career = builder.career;
    }


    public static class BuliderPerson {
        //    需要创建的部分
        private final String name;
        private final String gender;
        private String age;
        private String shoes;
        private String clothes;
        private String money;
        private String house;
        private String car;
        private String career;


        public BuliderPerson(String name, String gender) {
            this.name = name;
            this.gender = gender;
        }

        public BuliderPerson setAge(String age) {
            this.age = age;
            return this;

        }

        public BuliderPerson setShoes(String shoes) {
            this.shoes = shoes;
            return this;
        }

        public BuliderPerson setClothes(String clothes) {
            this.clothes = clothes;
            return this;
        }

        public BuliderPerson setMoney(String money) {
            this.money = money;
            return this;
        }

        public BuliderPerson setHouse(String house) {
            this.house = house;
            return this;
        }

        public BuliderPerson setCar(String car) {
            this.car = car;
            return this;
        }

        public BuliderPerson setCareer(String career) {
            this.career = career;
            return this;
        }

        public Person buliderPer() {
            return new Person(this);
        }
    }
}