package builder;

import builder.house.basics.Foundation;
import builder.house.basics.Renovation;
import builder.house.basics.Roof;
import builder.house.basics.Wall;

public class House {
//    为了理解方便。不写get、set方法了。直接赋值

//    地基
    Foundation foundation;
//墙壁
    Wall wall;

//屋顶
    Roof roof;
//    装修
    Renovation renovation;

}

