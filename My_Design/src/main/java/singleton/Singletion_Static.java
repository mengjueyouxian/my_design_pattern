package singleton;

//静态内部类模式
public class Singletion_Static {

    private Singletion_Static() {
    }

    private static class demo {
        private final static Singletion_Static singletion_Static = new Singletion_Static();
    }

    public static Singletion_Static getInstance() {
        return demo.singletion_Static;
    }

}
