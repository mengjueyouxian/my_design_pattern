package singleton;

//DCL 懒汉模式
//1.为什么要使用volatile
//锁值保证可见性和原子性。并不保证有序性。可能会导致对象和锁进行从排序

//2.为什么要使用dcl两次判断。只用一次不行吗？
//最外边的一层是用来快速判断的.
//   并且类加载的时候存在版初始化的情况,就是赋值完成,但是并没有初始化的状态.
//    半初始化?
//        是因为从排序?还是类加载后的初始化为完成?
//3.有了dcl后为什么不是最佳实践？

public class Singletion_DCL {
    private static volatile   Singletion_DCL singletion_DCL;

    private Singletion_DCL() {
    }

    public static Singletion_DCL getSingleton_demo() {
        if (singletion_DCL == null) {
            synchronized (Singletion_DCL.class) {
                if (singletion_DCL == null) {
                    singletion_DCL = new Singletion_DCL();
                }
            }
        }

        return singletion_DCL;
    }
}
