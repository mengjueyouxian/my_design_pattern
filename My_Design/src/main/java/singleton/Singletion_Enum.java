package singleton;

/*
1.枚举允许继承类吗？
2.枚举允许实现接口吗？
3.枚举可以用等号比较吗？
4.可以继承枚举吗？
5.枚举可以实现单例模式吗？
6. 当使用compareTo()比较枚举时，比较的是什么？
7. 当使用equals()比较枚举的时候，比较的是什么？
* */
public enum Singletion_Enum     {

    APPLE(1),ORANGE(2),BANANA(3);
    int code;

      Singletion_Enum(int code){
        this.code=code;
    }
    public int getCode(){
          return code;
    }
 }
