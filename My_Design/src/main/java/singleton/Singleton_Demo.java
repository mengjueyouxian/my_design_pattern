package singleton;
//方法一：饥饿模式
//dcl需要使用volatile。因为锁，只有
public class Singleton_Demo {

    private static final Singleton_Demo singleton_demo=new Singleton_Demo();
    private Singleton_Demo(){

    }
    public static Singleton_Demo getSingleton_demo() {
        return singleton_demo;
    }


}
