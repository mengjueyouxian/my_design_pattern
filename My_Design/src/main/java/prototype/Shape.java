package prototype;

import java.io.Serializable;
import java.util.HashMap;

public   class Shape implements  Cloneable , Serializable {
private String name ;
private String type ;

//引用对象
public HashMap tHas=new HashMap<String,String>();

    @Override
    protected Object clone() throws CloneNotSupportedException {

         Shape shape= (Shape) super.clone();
         this.tHas= (HashMap) shape.tHas.clone();
        return shape;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Shape(String name, String type) {
        this.name = name;
        this.type = type;
    }
}
