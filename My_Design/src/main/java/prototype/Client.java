package prototype;

public class Client {
    public static void main(String[] args) throws CloneNotSupportedException {
//        测试。两个对象。克隆
//        对象1
        Shape shape=new Shape("多利","公羊")  ;
        System.out.println(shape.getName()+"|| "+shape.getType());
        shape.tHas.put("1","克隆1");
//        克隆羊
//        Shape shapeClone= (Shape) shape.clone();
//        System.out.println(shapeClone.getName()+"|| "+shapeClone.getType());
 /*输出结果
 * 多利|| 公羊
 * 多利|| 公羊
 * */


        Shape shapeClone= (Shape) shape.clone();
        System.out.println(shapeClone.getName()+"|| "+shapeClone.getType()+"||"+shapeClone.tHas.get("1"));
// 深度克隆测试
//        更改
        shapeClone.tHas.put("1","美美");

        System.out.println(shape.tHas.get("1"));

    }
}
