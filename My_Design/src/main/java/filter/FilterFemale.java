package filter;

import java.util.ArrayList;
import java.util.List;
//filter链表
public class FilterFemale implements Filter {
    private Filter criteria;
    private Filter otherCriteria;
//    添加过滤器
    public FilterFemale(Filter criteria, Filter otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }
//使用过滤器执行所有需要过滤的对象
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> firstCriteriaPersons = criteria.meetCriteria(persons);
        return otherCriteria.meetCriteria(firstCriteriaPersons);
    }
}
