package serializable_demo;

import singleton.Singletion_Enum;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {
    public static void main(String [] args)
    {

        Singletion_Enum e=null;
        try
        {
            FileInputStream fileIn = new FileInputStream("D:\\temp\\11.txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (Singletion_Enum) in.readObject();
            in.close();
            fileIn.close();
        }catch(IOException i)
        {
            i.printStackTrace();
            return;
        }catch(ClassNotFoundException c)
        {
            System.out.println("Employee class not found");
            c.printStackTrace();
            return;
        }
        System.out.println("Deserialized Employee...");
        System.out.println(e.getCode());
    }
}
