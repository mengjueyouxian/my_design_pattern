package serializable_demo;

import singleton.Singletion_Enum;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeDemo {
    public static void main(String [] args)
    {
//        Singletion_Enum e = ;

        try
        {
            FileOutputStream fileOut =
                    new FileOutputStream("D:\\temp\\11.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(Singletion_Enum.APPLE);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in /tmp/employee.ser");
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }
}
