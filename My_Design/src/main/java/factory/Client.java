package factory;

public class Client {
    public static void main(String[] args) {
        Factory factory=new Factory();
//        创建宝马车
        Car bmCar=factory.createFactory("BM");
//        创建车
        bmCar.createCar();
//        创建奔驰车
        Car bcCar=factory.createFactory("BC");
        bcCar.createCar();

    }
}
