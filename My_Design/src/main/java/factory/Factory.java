package factory;

public class Factory {

    public Car createFactory( String brand){
        Car car =null;
        if ("BM".equals(brand)){
            car=new BWCar();
        }else if("BC".equals(brand)){
            car=new BCCar();
        }
        return car;
    }
}
