package abstract_factory;
//工厂的实现
public class FactoryExtends extends  Abstract_Factory{



    public Car makeCar(String model) {
        Car car =null;
        if("BM".equals(model)){
            car=new BMCar();
        }
        return car;
    }

    public Tank makeTank(String model) {
        Tank tank=null;
        if("TK".equals(model)){
            tank=new BigTank();
        }

        return tank;
    }
}
