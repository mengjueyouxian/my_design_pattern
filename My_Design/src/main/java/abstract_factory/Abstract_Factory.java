package abstract_factory;
//抽象工厂
public abstract  class Abstract_Factory {
//    创建汽车
    public abstract Car makeCar(String model);
//    创建坦克
    public abstract  Tank makeTank(String model);
}
