package abstract_factory;
//调用工厂制作坦克和汽车
public class Client {
    public static void main(String[] args) {
//        创造两组不同的产品
//        产品1
        Abstract_Factory abstract_factory=new FactoryExtends();
        Car bmCar= abstract_factory.makeCar("BM");
        bmCar.createCar();

//产品2
        Tank tank=abstract_factory.makeTank("TK");
        tank.createTank();
    }



}
